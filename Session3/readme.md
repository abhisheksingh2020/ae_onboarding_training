# Starting off Week 3
___


* Using enviroment variables to parse in parameter and understanding 3rd party plugins like typesafe
* Understanding variable configs (Task 2)
* Getting access to Bitbucket and Jira.
* Cloning the important repositories
* Understanding Task 3

## Using enviroment variables to parse in parameter:
![envparse](https://i.ibb.co/F7VdF6q/Screenshot-19.png)


### Path and config variables can be saved to environment varaibles, data like:
- Local path
- AWS S3 path
- AWS key-id pairs

We can use third party plugin like  *_typesafe_*
[typesafe](https://lightbend.github.io/config/ "typesafe docs")

We can use edit configurations for more stuff like (Will be useful for task 3 setup)
- VM options
- Program configurations

![pconfig](https://i.ibb.co/N28f3qc/Screenshot-20.png)

This will be the task 2 (Understanding ho you can use path variables and environment variables to setup configurations which are needed)
___
## Getting Access to Bitbucket and JIRA

We need to follow a mthod to get Bitbucket and JIRA approval **IT Helpdesk** for the same contact your mentor.




## Cloning the repositories needed.

- flow-builder
- oe_test_spark_execution
- fb_utils
- ae_oe_utilities
- opticloud-java-sdk
- pa-java
- analytics_engine

These repositories can be found on :

[Bitbucket](https://bitbucket.org/dashboard/overview)

## Cloning

![bitbucketclone](https://i.ibb.co/yggY2sw/bitbucket.png)

after cloning each repository seperately we can start the software build process in the next week

![allrepos](https://i.ibb.co/JrP99SC/Capturerepo.png)
___

## Understanding Task 3

Your next task will be to understand what is flowbuilder and how to execute flows.

### Some important courses to go through before starting task 3

* [CCM Orchestration engine 1](https://mylearning.zs.com/ekp/servlet/ekp?CID=EKP002107960&LANGUAGE_TAG=0&TX=FORMAT1&POPUP=Y&PX=N&USERID=&SK= "CCM Orchestration engine 1")

* [CCM Orchestration engine 2](https://mylearning.zs.com/ekp/servlet/ekp?CID=EKP002108344&LANGUAGE_TAG=0&TX=FORMAT1&POPUP=Y&PX=N&USERID=&SK= "CCM Orchestration engine 2")

___

## Understanding the task
  Below is your excel task details – 
Sample excel you can get from analytics_engine repo that you must have already cloned -> _\analytics_engine\fs-flows

- 1. Read data from Json using connectors

    Sample:

        {
        "connectors": {
                    "metric_score": {
            "path": "c:/path/",
            "header": true,
            "inferSchema": false,
            "format": "Parquet"
            },
            "user_alignment": {
            "path": "s3://aws-a0019-use1-00-d-shrd-ccm-0071z/DT_PI_${Product}/PI_data_files/hcp_filter_data",
            "header": true,
            "inferSchema": false,
            "format": "Parquet"
            },
                    "suggestion": {
            "path": "s3://aws-a0019-use1-00-d-shrd-ccm-0071z/DT_PI_${Product}/PI_data_files/resource_optimization_output",
            "header": true,
            "inferSchema": false,
            "format": "Parquet"
            }
                    }
        }

-    2.  Read 3 parquet files from the path mentioned in JSON

    - 	Metric score
    - 	Suggestion
    - 	User Alignment


Files Schema - 

    •	metric score-

    metricid, score
    string, double

    •	user alignment - 

    userid, geoid
    string, string

    •	suggestion

    suggestionid, insighttext,geoid,metricid
    string, string, string, string, string

- 3.   Apply join between all three files and produce the output in parquet format at output path mentioned in JSON with below columns---

suggestionid, insighttext, geoid, metricid, metricscore, userid

As part of this task, you need to create all required parquet and json files by yourself.



## _Sample Excel is inside fs-flows do check it out to understand how you can write excel for flows instead of a GUI Flowbuilder._