# Starting of Week 2
___f

* Setting up postgres and reading data from it.
* Understanding JDBC and starting a project in intelliJ
* Understanding Sparksession and Sparkcontext
* Understanding dataframe and dataset
* Cloning the project folder for Task 1
* Performing Read, creation and insertion of dataframe.

## Setting up postgres and reading data:
![pgadmin4](https://i.ibb.co/stKJ35H/pgadmin4.png)

Postgres for Windows can be downloaded through given link
## [pgadmin4](https://www.postgresql.org/download/windows/ "postgres download")

We will be creating Table under public schema.
___
## Understanding JDBC and starting a project in intelliJ

We will be using **JDBC Driver** for the Spark application to connect to the Database.
We need to add this string to **_build.sbt_** as a maven dependency

-     libraryDependencies += "org.postgresql" % "postgresql" % "42.2.19"

[JDBC Driver page for postgresql](https://mvnrepository.com/artifact/org.postgresql/postgresql "JDBC Driver page")


## Startig new SBT project under IntelliJ 

![IntelliJIdea](https://i.ibb.co/12n1jhx/Screenshot-12.png)

We will be using **_sbt_** to configure the project dependencies through maven repositories.

![IntelliJIdea](https://i.ibb.co/bQnQGqM/Screenshot-14.png)



___

## Understanding Sparksession and Sparkcontext

- [Spark Session documentation](https://spark.apache.org/docs/latest/api/java/org/apache/spark/sql/SparkSession.html)
- [Spark Context documentation](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/SparkContext.html)

- Reading data through jdbc from postgres
    
        val jdbcDF = spark.read
        .format("jdbc")
        .option("driver", "org.postgresql.Driver")
        .option("url", "jdbc:postgresql://127.0.0.1/postgres")
        .option("dbtable", "\"crud_tester1\"")
        .option("user","add your username here")
        .option("password","Add your password here")
        .load()
        
___

## Understanding Dataframe and tempview

* [Dataframe tutorial]("https://spark.apache.org/docs/latest/sql-programming-guide.html "dataframe tutorial")
* [Dataset tutorial]("https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/Dataset.html")
# Cloning this repository for task materials

>git clone https://abhisheksingh2020@bitbucket.org/abhisheksingh2020/ae_onboarding_training.git


### The demo taskproject is included inside Week2 folder as **_spark_postgresql_connection_**


# Performing Read,Insertion and creation of dataframes

### All the operations must be performed as seperate Scala .object by using following steps:
- Initiating a spark session
- Using spark read to or spark write to read/write data
- Saving the data to the postgres table eitheer by using datasets or dataframes.
- Closing the session