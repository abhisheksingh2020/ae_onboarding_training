import org.apache.spark.sql.SparkSession

object Create {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("postgres_connection_CRUD")
      .master("local")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    //Fetching data from csv to add new entry in dataframe
    val df = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("mode", "failfast")
      .option("path", "C:\\Projects\\spark_postgresql_connection\\input_crud.csv")
      .load()


    //copying dataframe to another table
    df.write
      .mode("append")
      .format("jdbc")
      .option("url","jdbc:postgresql://127.0.0.1/postgres")
      .option("dbtable","\"crud_tester1\"")
      .option("user","postgres")
      .option("password","postgres")
      .save()
    println("DB populated Successfully")

  }
}
