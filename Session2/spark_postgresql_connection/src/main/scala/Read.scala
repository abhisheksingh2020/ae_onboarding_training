import org.apache.spark.sql.SparkSession

object Read {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("postgres_connection_CRUD")
      .master("local")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    //Getting credentials from your system environment variables
    val USR = sys.env.get("USER").get
    val PWD = sys.env.get("PASSWORD").get

    //Data reading through JDBC
    val jdbcDF = spark.read
      .format("jdbc")
      .option("driver", "org.postgresql.Driver")
      .option("url", "jdbc:postgresql://127.0.0.1/postgres")
      .option("dbtable", "\"crud_tester1\"")
      .option("user",USR)
      .option("password",PWD)
      .load()
    jdbcDF.show()


  }
}
