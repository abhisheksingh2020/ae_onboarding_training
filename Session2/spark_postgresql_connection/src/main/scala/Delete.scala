import org.apache.spark.sql.SparkSession

object Delete {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("postgres_connection_CRUD")
      .master("local")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")


    //Data reading through JDBC
    val dropDF = spark.read
      .format("jdbc")
      .option("driver", "org.postgresql.Driver")
      .option("url", "jdbc:postgresql://127.0.0.1/postgres")
      .option("dbtable", "\"crud_tester1\"")
      .option("user", "postgres")
      .option("password", "postgres")
      .load()
    dropDF.show()
    dropDF.createOrReplaceTempView("deletion_table")
    println("Table Dropped successfully")
    spark.sql("""select user_id from deletion_table""".stripMargin).show()
  }

}
