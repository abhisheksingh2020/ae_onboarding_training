# Lets Get Going
___

* Clearing basics about language and framework.
* Installing dependencies and framework to get started. 
* Courses to be followed.
* Cloning this repository for task materials

## Language and framework:
![scala-spiral](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4wa7SwJdsyPxBh2-BvCMzmjSxxgtomacGpA&usqp=CAU)

**Scala** will be used in this project on a large scale.

**Spark** is a framework written in scala and can be used for data analytics.
___
## Installing dependencies and framework

We will be using **_IntelliJ IDEA_** for the development process, so it will be great to get familiar with the interface and set up.

![IntelliJIdea](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1n7Jz88DU0TJLgCiiPcmEgFCKM9UlEtD7Kw&usqp=CAU)

We will be using **_sbt_** to configure the project dependencies through maven repositories.

[Youtube Setup tutorial](https://www.youtube.com/watch?v=ACp2ioiTwQk&t=254s&ab_channel=AzarudeenShahul)

___

## Courses, documentations and topics to be followed

- [Spark-core documentation](https://spark.apache.org/docs/latest/)
- [Spark-SQL documentation](https://spark.apache.org/docs/latest/sql-programming-guide.html)
- [Udemy course](https://www.udemy.com/course/apache-spark-with-scala-hands-on-with-big-data/)
- Topics to be covered (primary need before any task)
    
        spark-core
        spark-dataframe
        spark-datasets
        spark-sql
        spark-jdbc-connection
___
# Cloning this repository for task materials

>git clone https://abhisheksingh2020@bitbucket.org/abhisheksingh2020/ae_onboarding_training.git
